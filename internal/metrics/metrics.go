package metrics

import (
	"time"

	"github.com/prometheus/client_golang/prometheus"
)

const (
	namespace = "gitlab"
)

// Prometheus metrics
var (
	BootTime = prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "issues_boot_time_seconds",
		Help:      "unix timestamp of when the service was started",
	})

	ClientErrors = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: namespace,
			Name:      "issues_client_errors_total",
			Help:      "Number of errors received from the GitLab API.",
		}, []string{"reason"})

	ClientRequests = prometheus.NewCounter(
		prometheus.CounterOpts{
			Namespace: namespace,
			Name:      "issues_client_requests_total",
			Help:      "Number of requests to the GitLab API.",
		})

	ExporterUp = prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "issues_up",
		Help:      "Whether the service is ready to receive requests or not",
	})

	IssueOpeningAgeHistogram = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Namespace: namespace,
			Name:      "issues_opening_age_hours",
			Help:      "Opened issues age distribution",
			// FIXME: this should move to the config
			Buckets: []float64{3, 6, 12, 24, 72, 168, 720, 2160},
		},
		[]string{"project_name", "state"})

	IssueClosingAgeHistogram = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Namespace: namespace,
			Name:      "issues_closing_age_hours",
			Help:      "Closed issues age distribution",
			// FIXME: this should move to the config
			Buckets: []float64{3, 6, 12, 24, 72, 168, 720, 2160},
		},
		[]string{"project_name"})

	IssueLastUpdateAgeHistogram = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Namespace: namespace,
			Name:      "issues_last_update_age_hours",
			Help:      "Last updates distribution",
			// FIXME: this should move to the config
			Buckets: []float64{3, 6, 12, 24, 72, 168, 720, 2160},
		},
		[]string{"project_name"})

	Issues = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "issues",
		Help:      "how many issues are in the project",
	}, []string{"project_name", "state", "label"})

	OldestIssue = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "issues_oldest_issue_seconds",
		Help:      "unix timestamp of the oldest issue that is still open",
	}, []string{"project_name"})
)

func init() {
	BootTime.Set(float64(time.Now().Unix()))
	ExporterUp.Set(0)
}
